// This file contains configuration information of the application.
// This file is changed when we change the application directory structure.

const path = require('path');

module.exports = {
  target: "web",
  entry: [
    "babel-polyfill",
    path.join(__dirname, "./app/index.js")
  ],
  src: path.join(__dirname, "./app"),
  outputPath: path.join(__dirname, "./build"),
  config:path.join(__dirname,"./harness/config")
};

import React from 'react'
import Header from "./Header"
import HeaderBanner from "./HeaderBanner"
import MainContent from "./MainContent"
import Footer from "./Footer"
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { Route, Link } from 'react-router-dom'
import {getDataAsync, types, reducer} from "./MainContent/InitRedux"
import Project from './Project';
// import '../styles/styles.css';
class KokoroApp extends React.Component{

  componentWillMount(){
    this.store = createStore(
    reducer,
    applyMiddleware(thunk)
    );
    this.store.dispatch(getDataAsync(types.A))
  }

  render(){
    console.log('Rendering KokoroApp')
    console.log(this.store)
    return (
      <div>
        <Header />
        <div className="container-fluid">
          <Route exact path="/" render={()=>{
            return (
              <div>
                <div className="container-fluid">
                  <HeaderBanner/>
                </div>
                <Link to="/home"> Home </Link>
              </div>
            )
          }}></Route>
          <Route exact path="/home"  render={()=>{
            return (
              <div>
                <div className="container-fluid">
                  <HeaderBanner/>
                </div>
                <MainContent store={this.store} />
              </div>
            )
          }}></Route>
        </div>
        <div className="container-fluid" id="project">
          <Route path="/project/:id" component={Project}
          ></Route>
        </div>
      </div>
    )
  }
}

export default KokoroApp;

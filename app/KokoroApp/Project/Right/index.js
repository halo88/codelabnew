import React from 'react';
import RightTop from './RightTop';
import Content from './Content';

export default class Right extends React.Component{
  render(){
    return (
      <div>
        <div className="row top-part">
          <RightTop/>
        </div>
        <div className="row content-part">
          <Content/>
        </div>
      </div>
    )
  }
}

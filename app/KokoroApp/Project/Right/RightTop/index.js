import React from 'react';

export default class RightTop extends React.Component{
    render(){
      return (
        <div className="right-top">
          <div className="top-top">
            <h2 className="right-title">Accelerated Mobile Pages Advanced</h2>
          </div>
        </div>
      )
    }
}

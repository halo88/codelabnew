import React from 'react';

export default class Content extends React.Component{
  render(){
    return (
      <div className='content'>
        <div className="inner">
            <h2>1. Overview</h2><br/>
              <p className="paragraph">
              This codelab is a continuation of the concepts introduced in Accelerated Mobile
              Pages Foundations. You should already have completed the previous code lab before
              starting this lab or at least have a basic understanding AMP's core concepts already.<br/>

              In this codelab, you'll learn how AMP handles advertising, analytics, video embedding,
               social media integration, image carousels and more. To achieve this you will build
                upon the example from the Foundations codelab by adding these features via the
                 various AMP components.<br/>
               </p>
            <h2>What you'll learn</h2><br/>
              <p className="paragraph">
              Display ads with amp-ad.<br/>
              Embed Youtube videos, Twitter cards and responsive text elements.<br/>
              Build carousels with images and combinations of content using amp-carousel.<br/>
              Simple tracking patterns with amp-analytics.<br/>
              Ways to add site navigation to your page.<br/>
              How fonts work with AMP.<br/>
              <h2>What you'll need</h2><br/>
              <ul>
                <li>The sample code</li>
                <li>Chrome (or an equivalent browser that can inspect the JavaScript console)</li>
                <li>Python (preferably 2.7) or the Chrome 200 OK Web Server extension</li>
                <li>Code Editor (for example Atom, Sublime, Notepad++)</li>
              </ul>
              </p>
        </div>
      </div>
    )
  }
}

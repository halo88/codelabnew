import React from 'react';
import Left from './Left';
import Right from './Right';
import '../../styles/bootstrapless/bootstrap.less';
import '../../styles/project.less';

export default class Project extends React.Component{
  render(){
    return (
      <div>
        <div className="row">
          <div className="col-md-2 col-xs-2 col-sm-2 col-lg-2 left">
              <Left/>
          </div>
          <div className="col-md-10 col-xs-10 col-sm-10 col-lg-10 right">
              <Right/>
          </div>
        </div>
      </div>
    )
  }
}

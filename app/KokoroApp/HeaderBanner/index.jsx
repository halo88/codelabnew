import React from 'react'

const HeaderBanner = ()=>{
  return (
            <div>
                <div className="row headerbanner">
                    <div className="col-md-offset-1 col-md-8">
                        <h2 className="welcome-header">Welcome to Codelabs!</h2>
                        <div className="welcomebody">Google Developers Codelabs provide a guided, tutorial, hands-on
                            coding
                            experience.
                            Most codelabs will step you through the process of building a small application,
                            or adding a new feature to an existing application.
                            They cover a wide range of topics such as Android Wear,
                            Google Compute Engine, Project Tango, and Google APIs on iOS.
                        </div>
                    </div>
                </div>
            </div>
        )
}
export default HeaderBanner

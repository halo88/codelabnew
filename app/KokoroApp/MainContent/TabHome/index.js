import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import {types} from "../InitRedux"
import axios from 'axios'
import {actionCreator} from "../InitRedux"
import GridItem from './GridItem';
import {getDataAsync} from "../InitRedux"
// import '../../../styles/styles.css';

const styles = {
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
    tab: {
        width: '30%'
    }
};

export default class TabHome extends React.Component {

    constructor(props) {
        super(props);
    }

    handleChange = function(value, empty, prop){
      let store = prop.props.store;
        switch (value) {
            case 'A': {
                store.dispatch(getDataAsync(types.A));
                break;
            }
            case 'B': {
                store.dispatch(getDataAsync(types.B));
                break;
            }
            case 'C': {
                store.dispatch(getDataAsync(types.C));
                break;
            }
        }
    };

    render() {
        console.log('rendering TabHome');
        const store = this.props.store;
        const list = store.getState().list;
        let tempList = [];
        for (let id in list){
          tempList.push({...list[id], key:id})
        }
        return (
            <div className="row site-width site-bot">

                <Tabs onChange={this.handleChange}>
                    <Tab label="A-Z" style={styles.tab} store={store} value="A">

                    </Tab>
                    <Tab label="Recent" style={styles.tab} store={store} value="B">

                    </Tab>
                    <Tab label="Duration" style={styles.tab} store={store} value="C">

                    </Tab>

                </Tabs>
                <div id="cards" className="site-width">
                {tempList.map(item =>{
                  return <GridItem key={item.key} item={item}/>
                })}
                </div>
            </div>
        );
    }
}

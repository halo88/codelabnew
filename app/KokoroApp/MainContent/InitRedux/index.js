import axios from 'axios';

export const types = {
    A: "Alphabet",
    B: "Recent",
    C: "Duration"
}
function Project(name, duration, createTime, desc, updateTime){
  this.name = name;
  this.duration = duration;
  this.createTime = createTime;
  this.desc = desc;
  this.updateTime = updateTime;
}

export function getDataAsync(type) {
    console.log('type:' + type);
    return function (dispatch) {

        return axios.get('http://localhost:3030/api/getList' + type).then(res => {
          dispatch(actionCreator(type, res.data));
        }, error => {
            console.log(error)
        })
    }
}

export function actionCreator(type, list) {
    console.log('begin action creator')
    // console.log(list)
    switch (type) {
        case types.A: {
            //get list sorted Alphabet
            console.log('get list sorted Alphabet');
            // console.log(list)
            return {
                list: list,
                type: "Alphabet"
            };
        }
        case types.B: {
            console.log('get list sorted Recent');
            //get list sorted Recent
            return {
                list: list,
                type: "Recent"
            }
        }
        case types.C: {
            console.log('get list sorted Duration')
            return {
                list: list,
                type: "Duration"
            };
        }
        default:
            console.log("default");
    }
}

export const reducer = (state, action) => {
  console.log('call reducer');
    if (state === undefined && action.list === undefined) {
        console.log('state undefined')
        return {
            list: {
              gg:{
                name: '',
                duration: '',
                createTime: '',
                desc: '',
                updateTime: 'new Date()'
            }
          },
            sort: 'None'
        }
    }
    console.log("list init: ")
    const list = action.list;
    return {
        ...state,
        sort: action.type,
        list: list
    };
}

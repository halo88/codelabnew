import React from "react"
import {Route, Switch} from "react-router-dom"
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TabHome from './TabHome'
require('../../styles/style.less');

class MainContent extends React.Component{

  constructor(props){
    super(props);
  }

  componentWillMount(){
    this.store = this.props.store;
    const list = this.store.getState().list;
    this.setState( {list : list} );
    this.unsubscribe = this.store.subscribe(()=>{
      this.setState({ list:list });
    })
  }

  componentWillUnmount(){
    this.unsubscribe();
  }

  render(){
    return (
              <div>
                  <div className="container-fluid part">
                      <MuiThemeProvider>
                        <TabHome store={this.store}/>
                      </MuiThemeProvider>
                  </div>
              </div>
          )
  }
}

export default MainContent;

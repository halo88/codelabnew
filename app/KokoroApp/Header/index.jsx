import React from "react"
import { Link } from 'react-router-dom'

const Header = () => {
  return (
      <div id="mainToolbar" className="paper-header">
          <div className="site-width layout horizontal">
          <Link to="/">
                  <img src="https://codelabs.developers.google.com/images/developers-logo.svg"
                       className="logo-icon"/>
                  <img src="https://codelabs.developers.google.com/images/lockup_developers_light_color.svg"
                       className="logo-devs"/>
            </Link>
              <div className="search">
                  <button className="searchBtn"><span className="glyphicon glyphicon-search"></span></button>
                  <input className="searchInput" placeholder="Search"/>
              </div>
          </div>
      </div>
  )
}

export default Header;

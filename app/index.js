import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';

import createHistory from 'history/createBrowserHistory';

import KokoroApp from './KokoroApp';
// import './styles/styles.css'
require('./styles/bootstrapless/bootstrap.less');


const MOUNT_NODE = document.getElementById('app');

const history = createHistory();

import { BrowserRouter } from 'react-router-dom'
const render = () =>  {
  ReactDOM.render(
    <BrowserRouter>
      <KokoroApp />
    </BrowserRouter>,
    MOUNT_NODE
  );
}

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept('./KokoroApp', () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render();
  });
}

render();

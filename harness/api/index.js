var express = require('express');
var app = express();
function Project(name, duration, createTime, desc, updateTime){
  this.name = name;
  this.duration = duration;
  this.createTime = createTime;
  this.desc = desc;
  this.updateTime = updateTime;
}
app.listen(3030, function(error, a){
  console.log('server api started on port 3030');
  console.log(arguments);
})
app.use((req, res, next)=>{
  res.header('Access-Control-Allow-Origin', "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
})
app.get('/api/getListAlphabet',(req,res)=>{
  let fakedata = {
    a: new Project('project 1', 30, new Date(), 'desc for project 1', new Date()),
    b: new Project('project 2', 30, new Date(), 'desc for project 2', new Date()),
    c: new Project('project 3', 30, new Date(), 'desc for project 3', new Date()),
    d: new Project('project 4', 30, new Date(), 'desc for project 4', new Date()),
    e: new Project('project 5', 30, new Date(), 'desc for project 5', new Date()),
    f: new Project('project 6', 30, new Date(), 'desc for project 6', new Date()),
  }
  res.json({
    ...fakedata,
  });
})
app.get('/api/getListRecent',(req,res)=>{
  let fakedata = {
    a: new Project('project 1', 30, new Date(), 'desc for project 1', new Date()),
    b: new Project('project 2', 30, new Date(), 'desc for project 2', new Date()),
    c: new Project('project 3', 30, new Date(), 'desc for project 3', new Date()),
    d: new Project('project 4', 30, new Date(), 'desc for project 4', new Date()),
    e: new Project('project 5', 30, new Date(), 'desc for project 5', new Date()),
    f: new Project('project 6', 30, new Date(), 'desc for project 6', new Date()),
  }
  res.json({
    ...fakedata,
  });
})
app.get('/api/getListDuration',(req,res)=>{
  let fakedata = {
    a: new Project('project 1', 30, new Date(), 'desc for project 1', new Date()),
    b: new Project('project 2', 30, new Date(), 'desc for project 2', new Date()),
    c: new Project('project 3', 30, new Date(), 'desc for project 3', new Date()),
    d: new Project('project 4', 30, new Date(), 'desc for project 4', new Date()),
    e: new Project('project 5', 30, new Date(), 'desc for project 5', new Date()),
    f: new Project('project 6', 30, new Date(), 'desc for project 6', new Date()),
  }
  res.json({
    ...fakedata,
  });
})

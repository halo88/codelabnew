// This file contains configuration based on webpack configuration https://webpack.js.org/configuration/

const projectConfig = require('../../project.config');
const devConfig = require('./webpack.config.dev');
const prodConfig = require('./webpack.config.prod');

const path = require('path');
const merge = require('webpack-merge');

const commonConfig = {
  entry: projectConfig.entry,
  output: {
    path: projectConfig.outputPath,
    publicPath: '/',
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[chunkhash].js',
  },
  resolve: {
    extensions: [
      '.js',
      '.jsx',
    ],
  },
  context: __dirname,
  target: projectConfig.target,
}


module.exports = function(env) {
  console.log('environment: ' + env);

  const config = env === 'production' ?
    prodConfig : devConfig;

  return merge([commonConfig, config]);
};

// This file contains configuration for development environment
// Change the values for each options in this file

const merge = require("webpack-merge");
const parts = require("./webpack.config.parts");

module.exports = merge([
  parts.enableHMR(),
  parts.indexTemplate(),
  parts.loadJavaScript(),
]);

const path = require('path');
const projectConfig = require("../../project.config");
const webpack = require('webpack');
// const ExtractTextPlugin = require("extract-text-webpack-plugin");

const HtmlWebpackPlugin = require('html-webpack-plugin');
exports.indexTemplate = function() {
  return {
    plugins: [
      new HtmlWebpackPlugin({
         inject: true,
         template: path.join(projectConfig.src,"index.html"),
      })
    ]
  };
}

exports.loadJavaScript = function() {
  return {
    module: {
      rules: [
        {
          test: /\.js$|\.jsx$/,
          include:[
            projectConfig.src
          ],
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
                presets: [
                  [
                    "env",
                    {
                      "modules": false
                    }
                  ],
                  "react",
                  "stage-0"
                ],
              }
            },
          ],
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader',
            },{
              loader: 'css-loader',
            }

          ]
        },
        {
          test: /\.less$/,
          use: [{
                loader: "style-loader"
            }, {
                loader: "css-loader"
            }, {
                loader: "less-loader"
            }]
        },
        {
          test: /\.(ttf|svg|woff|woff2|eot)$/,
          use: [{
                loader: "file-loader"
            },]
        }
      ],
    },
  };
};

exports.enableHMR = function() {
  return {
    entry: [
      'eventsource-polyfill', // Necessary for hot reloading with MS Edge
      'webpack-hot-middleware/client?reload=true',
    ],
    devtool: 'eval-source-map',
    plugins: [
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.NamedModulesPlugin(),
    ],
  };
}

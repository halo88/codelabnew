const addProdMiddlewares = require('./addProdMiddlewares');
const addDevMiddlewares = require('./addDevMiddlewares');

const path = require('path');
const projectConfig = require('../../../project.config');
const webpackConfig = require(path.join(projectConfig.config, "webpack.config"))(process.env.NODE_ENV);

module.exports = (app) => {
  const isProd = process.env.NODE_ENV === 'production';
  if (isProd) {
    addProdMiddlewares(app, webpackConfig);
  } else {
    addDevMiddlewares(app, webpackConfig);
  }

  return app;
};

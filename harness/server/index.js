const path = require('path');
const express = require('express');

const projectConfig = require('../../project.config');

const setup = require('./middlewares/frontendMiddleware');

const app = express();

setup(app);

const host = 'localhost';
const port = 3000;

app.listen(port, host, (err) => {
  if (err) {
    return logger.error(err.message);
  }
  console.log('Running on ' + host + ':' + port);
});
